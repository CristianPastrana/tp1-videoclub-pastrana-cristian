from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlite3 import Connection as SQLite3Connection

from servicios import db

import uuid

@event.listens_for(Engine, "connect")
def _set_sqlite_pragma(dbapi_connection, connection_record):
    if isinstance(dbapi_connection, SQLite3Connection):
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()
        



class Client(db.Model):
    client_id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(30), nullable=False)
    apellido = db.Column(db.String(30), nullable=False)
    telefono = db.Column(db.Integer, nullable=False)
    domicilio = db.Column(db.String(30), nullable=False)

class Rent(db.Model):
    object_id = db.Column('id', db.Text(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)  
    client_id = db.Column(db.Integer)
    status = db.Column(db.String(10), nullable=False)
    until = db.Column(db.String(10), nullable=False)

