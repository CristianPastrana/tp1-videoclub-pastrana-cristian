# TP1-Pastrana Cristian V2

**Trabajo práctico inicial**

**Objetivo**


El objetivo es ir conociendo algunas herramientas y visitar algunos conocimientos ya adquiridos.  Para esto construiremos una pequeña API REST.

**¿Qué tenemos qué hacer?**

Un ataque de nostalgia hace que tengamos que preparar un sistema para un videoclub. ¿Qué es? En un momento era un negocio donde uno alquilaba películas por una cantidad de tiempo y luego tenía que devolverlas. Eso sí, rebobinadas. 

Nuestra misión es empezar un sistema para procesar todo ese negocio.

**¿Que necesitamos?**
- Python 2.7.x > https://www.python.org/download/releases/2.7/
- Postman > https://www.postman.com/downloads/

**¿Como ejecuto el programa?**

-Primero instalaremos virtualenv, el cual nos permitira crear nuestro entorno virtual para el proyecto.

En Windows
> \Python27\python.exe -m pip install virtualenv

En linux
> sudo apt-get install python-virtualenv

- Clonamos el repositorio en la carpeta Scripts donde tengamos instalado Python2.7 ( Por lo general c:/Python27/Scripts)
> \Python27\Scripts\git clone https://gitlab.com/CristianPastrana/tp1-pastrana-cristian-v2.git


- Crearemos un entorno virtual para nuestro proyecto sobre el repositorio que acabamos de clonar de la siguiente manera:

> \Python27\Scripts\virtualenv.exe tp1-pastrana-cristian-v2

-Activamos el entorno virtual que acabamos de crear.

> \Python27\Scripts\tp1-pastrana-cristian-v2\Scripts\activate

- Importamos las dependencias necesarias para el proyecto

> \Python27\Scripts\tp1-pastrana-cristian-v2\pip install -r requirements.txt

- Ejecutamos server.py, el cual iniciara el servidor para nuestra aplicacion.

> \Python27\Scripts\tp1-pastrana-cristian-v2\python.exe server.py

   ![Captura5](/uploads/f671382143202c49170fef5da9fbd1c2/Captura5.PNG)

- Luego en otra terminal, activaremos de nuevo el entorno virtual para ejecutar un test.
>  Python27\Scripts\tp1-pastrana-cristian-v2\Scripts\activate

> \Python27\Scripts\tp1-pastrana-cristian-v2\python.exe api_test.py

   ![Captura6](/uploads/307234be5092a645dc207d6e536cfae8/Captura6.PNG)

- Si realizamos los pasos correctamente, habremos iniciado el servidor y realizado un test unitario a nuestra api.

**¿Como relizo pruebas con Postman?**

- Abrimos el Postman.

- Importamos el archivo **Pruebas API.postman_collection**

- Nos aparecera un listado con los requests.

- Ejecutamos los requests.



